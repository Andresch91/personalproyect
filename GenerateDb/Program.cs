﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

using Modelos;
using DB;

namespace GenerateDb
{
    class Program
    {
        static void Main(string[] args)
        {
            var Categoria01 = new Categoria()
            {
                Descripcion = "Bebidas"
            };

            var producto01 = new Producto() 
            {
            Nombre="Coca-Cola 3Lt",
            Marca="Coca-Cola",
            Precio= 8            
            };

            var _context = new ProyectContext();

            Console.WriteLine("Creando DB");

            _context.Categorias.Add(Categoria01);
            _context.Productos.Add(producto01);

            _context.SaveChanges();

            Console.WriteLine("Base de Datos Creada....");
            Console.ReadLine();

        }
    }
}
