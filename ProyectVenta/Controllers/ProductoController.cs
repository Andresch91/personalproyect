﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.Mvc;
using Modelos;
using DB;
using Interfaces;
using Repository;
using Validaciones;

namespace ProyectVenta.Controllers
{
    public class ProductoController : Controller
    {
        private IProducto repo;
        private ICategoria repoCat;
        private ValidatorProducto validacion;

        public ProductoController(ProductoRepository repo, ICategoria repoCat, ValidatorProducto validacion)
        {
            this.repo = repo;
            this.validacion = validacion;
            this.repoCat = repoCat;

        }

        [HttpGet]
        public ViewResult Index(string criterio="")
        {
            var datos = repo.AllNombre(criterio);
            return View("Inicio", datos);
        }

    }
}
