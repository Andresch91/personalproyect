﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.Mvc;
using Modelos;
using Repository;
using Interfaces;
using DB;
using Validaciones;


namespace ProyectVenta.Controllers
{
    public class CategoriaController : Controller
    {
        // GET: /Categoria/
        private ICategoria Crepo;
        private ValidatorCategoria Vcategoria;

        public CategoriaController(CategoriaRepository Crepo, ValidatorCategoria Vcategoria)
        {
            this.Crepo = Crepo;
            this.Vcategoria = Vcategoria;
        }

        [HttpGet]
        public ViewResult Index(string criterio="")
        {
            var datos = Crepo.AllNombre(criterio);
            return View("Inicio", datos);
            
        }
        [HttpGet]
        public ViewResult Create()
        {
            return View("CreateCategoria");
        }

        [HttpPost]
        public ActionResult Create(Categoria categoria)
        {
            if (ValidatorCategoria.Pass(categoria))
            {
                Crepo.Store(categoria);
                TempData["UpdateSuccess"] = "Categoria guardada con exito";
                return RedirectToAction("Index");
            }

            return View("CreateCategoria", categoria);
        }

        [HttpGet]
        public ViewResult Edit(int id)
        {
            var data = Crepo.Find(id);
            return View("Editar", data);

        }

        [HttpPost]
        public ActionResult Edit(Categoria categoria)
        {
            Crepo.Update(categoria);
            TempData["UpdateSuccess"] = "Categoria Actualizada";
            return RedirectToAction("Index");
        }
        [HttpGet]
        public ActionResult Delete(int id)
        {
            Crepo.Delete(id);
            TempData["UpdateSuccess"] = "Categoria Eliminada";
            return RedirectToAction("Index");
        }
    }
}
