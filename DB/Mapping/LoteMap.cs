﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

using System.ComponentModel.DataAnnotations;
using System.ComponentModel.DataAnnotations.Schema;
using System.Data.Entity.ModelConfiguration;
using Modelos;


namespace DB.Mapping
{
   public class LoteMap:EntityTypeConfiguration<Lote>
    {
       public LoteMap()
       {
           this.HasKey(p => p.IdLote);
           this.Property(p => p.IdLote).HasDatabaseGeneratedOption(DatabaseGeneratedOption.Identity);

           this.Property(p => p.FechaVencimiento).IsRequired();
           this.Property(p => p.NroLote).IsRequired();
           this.Property(p => p.StockInicial).IsOptional();

           this.ToTable("Lote");
       }
    }
}
