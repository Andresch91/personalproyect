﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

using System.ComponentModel.DataAnnotations;
using System.ComponentModel.DataAnnotations.Schema;
using System.Data.Entity.ModelConfiguration;
using Modelos;

namespace DB.Mapping
{
  public  class DetalleVentaMap:EntityTypeConfiguration<DetalleVenta>
    {
      public DetalleVentaMap()
      {
          this.HasKey(p => p.IdDetalleVenta);
          this.Property(p => p.IdDetalleVenta).HasDatabaseGeneratedOption(DatabaseGeneratedOption.Identity);

          this.Property(p => p.Cantidad).IsRequired();
          this.Property(p => p.PrecioDv).IsRequired();
          this.Property(p => p.StockModificar).IsRequired();

          this.HasRequired(p => p.Producto).WithMany().HasForeignKey(p => p.IdProductofk).WillCascadeOnDelete(false);

          this.ToTable("DetalleVenta");

      }
    }
}
