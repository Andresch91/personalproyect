﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

using System.ComponentModel.DataAnnotations;
using System.ComponentModel.DataAnnotations.Schema;
using System.Data.Entity.ModelConfiguration;
using Modelos;

namespace DB.Mapping
{
  public  class StockMap:EntityTypeConfiguration<Stock>
    {
      public StockMap()
      {
          this.HasKey(p => p.IdStock);
          this.Property(p => p.IdStock).HasDatabaseGeneratedOption(DatabaseGeneratedOption.Identity);

          this.Property(p => p.StockActual).IsRequired();         

          this.HasRequired(p => p.Producto).WithMany().HasForeignKey(p => p.Idproductofk).WillCascadeOnDelete(false);
          this.HasRequired(p => p.Lote).WithMany().HasForeignKey(p => p.IdLotefk).WillCascadeOnDelete(false);

          this.ToTable("Stock");

      }
    }
}
