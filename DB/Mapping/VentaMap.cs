﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

using System.ComponentModel.DataAnnotations;
using System.ComponentModel.DataAnnotations.Schema;
using System.Data.Entity.ModelConfiguration;
using Modelos;

namespace DB.Mapping
{
   public class VentaMap:EntityTypeConfiguration<Venta>
    {
       public VentaMap()
       {
           this.HasKey(p => p.IdVenta);
           this.Property(p => p.IdVenta).HasDatabaseGeneratedOption(DatabaseGeneratedOption.Identity);

           this.Property(p => p.Fecha).IsRequired();
           this.Property(p => p.NroComprobante).HasMaxLength(20).IsRequired();

           this.HasRequired(p => p.Cliente).WithMany().HasForeignKey(p => p.IdClientefk).WillCascadeOnDelete(false);
           

           this.ToTable("Venta");
       }
    }
}
