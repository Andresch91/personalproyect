﻿using Modelos;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

using System.ComponentModel.DataAnnotations;
using System.ComponentModel.DataAnnotations.Schema;
using System.Data.Entity.ModelConfiguration;

namespace DB.Mapping
{
   public class ProveedorMap:EntityTypeConfiguration<Proveedor>
    {
       public ProveedorMap()
       {
           this.HasKey(p=>p.IdProveedor);
           this.Property(p=>p.IdProveedor).HasDatabaseGeneratedOption(DatabaseGeneratedOption.Identity);

           this.Property(p => p.Ruc).HasMaxLength(11).IsRequired();
           this.Property(p => p.RazonSocial).HasMaxLength(50).IsRequired();
           this.Property(p => p.Telefono).HasMaxLength(9).IsRequired();
           this.Property(p => p.Correo).HasMaxLength(50).IsRequired();
           this.Property(p => p.PaginaWeb).HasMaxLength(50).IsRequired();

           this.ToTable("Proveedor");
       }

    }
}
