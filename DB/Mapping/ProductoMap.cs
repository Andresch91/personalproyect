﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

using System.ComponentModel.DataAnnotations;
using System.ComponentModel.DataAnnotations.Schema;
using System.Data.Entity.ModelConfiguration;
using Modelos;

namespace DB.Mapping
{
   public class ProductoMap:EntityTypeConfiguration<Producto>
    {
       public ProductoMap()
       {
           this.HasKey(p => p.IdProducto);
           this.Property(p => p.IdProducto).HasDatabaseGeneratedOption(DatabaseGeneratedOption.Identity);

           this.Property(p => p.Nombre).HasMaxLength(50).IsRequired();
           this.Property(p => p.Marca).HasMaxLength(50).IsRequired();
           this.Property(p => p.Precio).HasPrecision(9, 2).IsRequired();
           this.Property(p => p.Stock).HasPrecision(9, 2).IsRequired();

           //Relacion fk
           this.HasRequired(p => p.Categoria)
               .WithMany().HasForeignKey(p => p.IdCategoriafk)
               .WillCascadeOnDelete(false);

           this.ToTable("Producto");
       }
    }
}
