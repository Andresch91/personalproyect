﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

using System.ComponentModel.DataAnnotations;
using System.ComponentModel.DataAnnotations.Schema;
using System.Data.Entity.ModelConfiguration;
using Modelos;

namespace DB.Mapping
{
   public class CategoriaMap:EntityTypeConfiguration<Categoria>
    {
       public CategoriaMap() 
       {
       this.HasKey(p=>p.IdCategoria);
       this.Property(p => p.IdCategoria).HasDatabaseGeneratedOption(DatabaseGeneratedOption.Identity);

       this.Property(p => p.Descripcion).HasMaxLength(100).IsRequired();

       this.ToTable("Categoria");
       }

    }
}
