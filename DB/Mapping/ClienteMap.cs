﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

using System.ComponentModel.DataAnnotations;
using System.ComponentModel.DataAnnotations.Schema;
using System.Data.Entity.ModelConfiguration;
using Modelos;

namespace DB.Mapping
{
   public class ClienteMap:EntityTypeConfiguration<Cliente>
    {
       public ClienteMap()
       {
           this.HasKey(p => p.IdCliente);
           this.Property(p => p.IdCliente).HasDatabaseGeneratedOption(DatabaseGeneratedOption.Identity);

           this.Property(p => p.Nombre).HasMaxLength(30).IsRequired();
           this.Property(p => p.Apellidos).HasMaxLength(50).IsRequired();
           this.Property(p => p.DNI).HasMaxLength(8).IsRequired();
           this.Property(p => p.Direccion).HasMaxLength(50).IsRequired();
           this.Property(p => p.Telefono).HasMaxLength(9).IsRequired();

           this.ToTable("Cliente");
           
       }
    }
}
