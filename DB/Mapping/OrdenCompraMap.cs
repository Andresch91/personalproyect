﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

using System.ComponentModel.DataAnnotations;
using System.ComponentModel.DataAnnotations.Schema;
using System.Data.Entity.ModelConfiguration;
using Modelos;

namespace DB.Mapping
{
  public  class OrdenCompraMap:EntityTypeConfiguration<OrdenCompra>
    {
      public OrdenCompraMap()
      {
          this.HasKey(p => p.IdOrdenCompra);
          this.Property(p => p.IdOrdenCompra).HasDatabaseGeneratedOption(DatabaseGeneratedOption.Identity);

          this.Property(p => p.Cantidad).IsRequired();
          this.Property(p => p.Precio).IsRequired();

          this.HasRequired(p => p.Producto).WithMany().HasForeignKey(p => p.IdProductofk).WillCascadeOnDelete(false);


          this.ToTable("Ordencompra");
      }
    }
}
