﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

using System.Data.Entity;
using Modelos;
using DB.Mapping;

namespace DB
{
   public class ProyectContext:DbContext
    {
       public ProyectContext()
       { 
       Database.SetInitializer(new DropCreateDatabaseIfModelChanges<ProyectContext>());
       }

       public DbSet<Producto> Productos { get; set; }
       public DbSet<Categoria> Categorias { get; set; }
       public DbSet<Cliente> Clientes { get; set; }
       public DbSet<Lote> Lotes { get; set; }
       public DbSet<Proveedor> Proveedores { get; set; }
       public DbSet<Stock> Stocks { get; set; }
       public DbSet<Venta> Ventas { get; set; }
       public DbSet<OrdenCompra> OrdenCompras { get; set; }
       public DbSet<DetalleVenta> DetalleVentas { get; set; }

       protected override void OnModelCreating(DbModelBuilder modelBuilder)
       {
           modelBuilder.Configurations.Add(new ProductoMap());
           modelBuilder.Configurations.Add(new CategoriaMap());
           modelBuilder.Configurations.Add(new ClienteMap());
           modelBuilder.Configurations.Add(new LoteMap());
           modelBuilder.Configurations.Add(new ProveedorMap());
           modelBuilder.Configurations.Add(new StockMap());
           modelBuilder.Configurations.Add(new VentaMap());
           modelBuilder.Configurations.Add(new OrdenCompraMap());
           modelBuilder.Configurations.Add(new DetalleVentaMap());
         

       }

    }
}
