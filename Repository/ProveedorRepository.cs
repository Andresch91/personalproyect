﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Data.Entity;

using Modelos;
using Interfaces;
using DB;

namespace Repository
{
   public class ProveedorRepository:MasterRepository, IProveedor
    {
        public List<Proveedor> All()
        {
            var query = from p in Context.Proveedores
                        select p;

            return query.ToList();
        }

        public List<Proveedor> AllNombre(string criterio)
        {
            var query = from p in Context.Proveedores
                        select p;
            if (!string.IsNullOrEmpty(criterio))
            {
                query = query.Where(p => p.RazonSocial.ToUpper()
                                    .Contains(criterio.ToUpper()) ||
                                    p.Ruc.ToUpper().Contains(criterio.ToUpper()));
            }
            return query.ToList();
        }

        public Proveedor Find(int id)
        {
            return Context.Proveedores.Find(id);
        }

        public void Store(Proveedor proveedor)
        {
            Context.Proveedores.Add(proveedor);
            Context.SaveChanges();
        }

        public void Update(Proveedor proveedor)
        {
            Context.Entry(proveedor).State = EntityState.Modified;
            Context.SaveChanges();
        }

        public void Delete(int id)
        {
            var existe = Context.Proveedores.Find(id);
            if (existe != null)
            {
                Context.Proveedores.Remove(existe);
                Context.SaveChanges();
            }
        }

        public List<Proveedor> ByQueryAll(string query)
        {
            throw new NotImplementedException();
        }

        public int GetMismoRuc(string ruc)
        {
            var query = from c in Context.Proveedores
                        where c.Ruc == ruc
                        select c;
            return (query).Count();
        }
    }
}
