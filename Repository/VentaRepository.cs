﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

using Modelos;
using Interfaces;
using DB;
namespace Repository
{
   public class VentaRepository:MasterRepository,Iventa
    {

        public List<Venta> All()
        {
            throw new NotImplementedException();
        }

        public Venta Find(int id)
        {
            return Context.Ventas.Find(id);
        }
       //Almacenar venta = Addventa
        public void Store(Venta venta)
        {
            var sale = new Venta();
            sale.IdVenta = venta.IdVenta;
            sale.NroComprobante = venta.NroComprobante;
            sale.TipoComprobante = venta.TipoComprobante;
            sale.Fecha = venta.Fecha;
            sale.Total = venta.Total;

            foreach (var item in venta.Detalleventa)
            {
                var rProducto = new ProductoRepository();
                var prodActualizado = rProducto.Find(item.IdProductofk);
                prodActualizado.Stock = prodActualizado.Stock + item.Cantidad;
                rProducto.Update(prodActualizado);
                venta.Total = venta.Total + (item.Cantidad * item.PrecioDv);

                var detalle = new DetalleVenta();
                detalle.IdVentafk = venta.IdVenta;
                detalle.IdProductofk = item.IdProductofk;
                detalle.Cantidad = item.Cantidad;
                detalle.PrecioDv = item.PrecioDv;
                sale.Detalleventa.Add(detalle);
            }
            sale.Total = venta.Total;
            Context.Ventas.Add(venta);
            Context.SaveChanges();
        }

        public void Update(Venta venta)
        {
            Context.Entry(venta).State = System.Data.Entity.EntityState.Modified;
            Context.SaveChanges();
        }

        public void Delete(int id)
        {
            var existe = Context.Ventas.Find(id);
            if (existe != null)
            {
                Context.Ventas.Remove(existe);
                Context.SaveChanges();
            }
        }

        public List<Venta> ByQueryAll(string query)
        {
            throw new NotImplementedException();
        }


        public List<Venta> AllNombre(string criterio)
        {
            var query = from v in Context.Ventas.Include("Cliente")
                        select v;
            if (!string.IsNullOrEmpty(criterio))
            {
                query = from p in query
                        where p.NroComprobante.ToUpper().Contains(criterio.ToUpper())
                        select p;
            }
            return query.ToList();
        }
    }
}
