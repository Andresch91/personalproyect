﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Data.Entity;

using Modelos;
using Interfaces;
using DB;

namespace Repository
{
  public  class LoteRepository:MasterRepository, ILote
    {
        public List<Lote> All()
        {
            var result = from p in Context.Lotes select p;
            return result.ToList();
        }

        public Lote Find(int id)
        {
            var result = from p in Context.Lotes.Include("Proveedor").Include("Producto") 
                         where p.IdLote == id select p;

            return result.FirstOrDefault();
        }

        public void store(Lote lote)
        {
            Context.Lotes.Add(lote);
            Context.SaveChanges();
        }

        public void Update(Lote lote)
        {
            var result = (from p in Context.Lotes where p.IdLote == lote.IdLote select p).First();


            result.NroLote = lote.NroLote;
            result.Idproductofk = lote.Idproductofk;
            result.StockInicial = lote.StockInicial;
            result.FechaVencimiento = lote.FechaVencimiento;
            result.IdProveedorfk = lote.IdProveedorfk;
            Context.SaveChanges();
        }

        public void Delete(int id)
        {
            var result = (from p in Context.Lotes
                          where p.IdLote == id select p).First();
            Context.Lotes.Remove(result);
            Context.SaveChanges();
        }

        public List<Lote> ByQueryAll(DateTime? date1, DateTime? date2)
        {
            var dbQuery = (from p in Context.Lotes.Include("Producto").Include("Proveedor") select p);


            if (date1 != null && date2 == null)
                dbQuery = dbQuery.Where(o => o.FechaVencimiento >= date1);

            if (date2 != null && date1 == null)
                dbQuery = dbQuery.Where(o => o.FechaVencimiento <= date2);

            if (date2 != null && date1 != null)
                dbQuery = dbQuery.Where(o => o.FechaVencimiento >= date1 && o.FechaVencimiento <= date2);

            return dbQuery.ToList();
        }
    }
}
