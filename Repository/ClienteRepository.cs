﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

using System.Data.Entity;
using Modelos;
using Interfaces;
using DB;

namespace Repository
{
   public class ClienteRepository:MasterRepository,ICliente
    {
        public List<Cliente> All()
        {
            var query = from c in Context.Clientes
                        select c;
            return query.ToList();
        }

        public List<Cliente> AllDni(string dni)
        {
            var query = from c in Context.Clientes
                        select c;
            if (!string.IsNullOrEmpty(dni))
            {
                query = query.Where(c => c.DNI.ToUpper().Contains(dni.ToUpper()));
               
            }
            return query.ToList();
        }

        public Cliente Find(int id)
        {
            return Context.Clientes.Find(id);
        }

        public void Add(Cliente cliente)
        {
            Context.Clientes.Add(cliente);
            Context.SaveChanges();
        }

        public void update(Cliente cliente)
        {
            Context.Entry(cliente).State = EntityState.Modified;
            Context.SaveChanges();
        }

        public void Delete(int id)
        {
            var existe = Context.Clientes.Find(id);

            if (existe != null)
            {
                Context.Clientes.Remove(existe);
                Context.SaveChanges();
            }
        }
    }
}
