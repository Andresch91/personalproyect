﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Data.Entity;

using Modelos;
using Interfaces;
using DB;

namespace Repository
{
   public class ProductoRepository:MasterRepository, IProducto
    {

        public List<Producto> All()
        {
            throw new NotImplementedException();
        }

        public List<Producto> AllNombre(string criterio)
        {
            var query = from p in Context.Productos.Include("Categoria")
                        select p;
            if (!string.IsNullOrEmpty(criterio))
            {
                query = from p in query
                        where p.Nombre.ToUpper().Contains(criterio.ToUpper()) ||
                              p.Categoria.Descripcion.ToUpper().Contains(criterio.ToUpper())
                        select p;

            }
            return query.ToList();
        }

        public Producto Find(int id)
        {
            return Context.Productos.Find(id);
        }

        public void Store(Producto producto)
        {
            Context.Productos.Add(producto);
            Context.SaveChanges();
        }

        public void Update(Producto producto)
        {
            Context.Entry(producto).State = EntityState.Modified;
            Context.SaveChanges();
        }

        public void Delete(int id)
        {
            var existe = Context.Productos.Find(id);
            if (existe != null)
            {
                Context.Productos.Remove(existe);
                Context.SaveChanges();
            }
        }

        public List<Producto> ByQueryAll(string query)
        {
            throw new NotImplementedException();
        }
    }
}
