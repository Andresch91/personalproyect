﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using DB;

namespace Repository
{
   public abstract class MasterRepository
    {
       private readonly ProyectContext _context;

       protected MasterRepository()
       {
           if (_context == null)
               _context = new ProyectContext();
       }

       protected ProyectContext Context
       {
           get { return _context; }
       }
    }
}
