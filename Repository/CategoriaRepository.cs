﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Data.Entity;
using Modelos;
using Interfaces;
using DB;

namespace Repository
{
    public class CategoriaRepository:MasterRepository, ICategoria
    {

        public IEnumerable<Categoria> All()
        {
            var query = from c in Context.Categorias
                        select c;
            return query;
        }

        public IEnumerable<Categoria> AllNombre(string descripcion)
        {
            var query = from c in Context.Categorias
                        select c;
            if (!string.IsNullOrEmpty(descripcion))
            {
                query = query.Where(c => c.Descripcion.ToUpper().Contains(descripcion.ToUpper()));
            }
            return query;
        }

        public void Store(Categoria categoria)
        {
            Context.Categorias.Add(categoria);
            Context.SaveChanges();
        }

        public void Update(Categoria categoria)
        {
            var resul = (from p in Context.Categorias
                         where p.IdCategoria == categoria.IdCategoria
                         select p).First();
            resul.Descripcion = categoria.Descripcion;
            Context.SaveChanges();
        }

        public void Delete(int id)
        {
            var existe = Context.Categorias.Find(id);
            if (existe != null)
            {
                Context.Categorias.Remove(existe);
                Context.SaveChanges();
            }  
        }

        public Categoria Find(int id)
        {
            var result = from p in Context.Categorias
                         where p.IdCategoria == id
                         select p;
            return result.FirstOrDefault();
            //return Context.Categorias.Find(id);
        }
    }     
}
