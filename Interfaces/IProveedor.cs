﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using Modelos;

namespace Interfaces
{
   public interface IProveedor
    {
       List<Proveedor> All();
       List<Proveedor> AllNombre(string criterio);
       Proveedor Find(int id);
       void Store(Proveedor proveedor);
       void Update(Proveedor proveedor);
       void Delete(int id);
       List<Proveedor> ByQueryAll(string query);
       int GetMismoRuc(string ruc);
    }
}
