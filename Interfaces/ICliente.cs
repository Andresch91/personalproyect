﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using Modelos;

namespace Interfaces
{
   public interface ICliente
    {
       List<Cliente> All();
       List<Cliente> AllDni(string dni);
       Cliente Find(int id);
       void Add(Cliente cliente);
       void update(Cliente cliente);
       void Delete(int id);
    }
}
