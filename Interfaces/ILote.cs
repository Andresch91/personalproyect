﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

using Modelos;

namespace Interfaces
{
  public  interface ILote
    {
      List<Lote> All();
      Lote Find(int id);
      void store(Lote lote);
      void Update(Lote lote);
      void Delete(int id);
      List<Lote> ByQueryAll(DateTime? date1, DateTime? date2);

    }
}
