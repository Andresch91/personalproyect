﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

using Modelos;

namespace Interfaces
{
  public  interface Iventa
    {
        List<Venta> All();
        List<Venta> AllNombre(string criterio);
        //Encontrar
        Venta Find(int id);
        //Almacenar
        void Store(Venta venta);
        void Update(Venta venta);
        void Delete(int id);
        //Consultar todo
        List<Venta> ByQueryAll(string query);
    }
}
