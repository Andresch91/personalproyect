﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using Modelos;

namespace Interfaces
{
   public interface ICategoria
    {
       //IEnumerable<Categoria> ByQueryAll(string query, string c, string ca);
       IEnumerable<Categoria> All();
       IEnumerable<Categoria> AllNombre(string descripcion);
       void Store(Categoria categoria);
       void Update(Categoria categoria);
       void Delete(int id);
       Categoria Find(int id);

    }
}
