﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

using Modelos;

namespace Interfaces
{
   public interface IProducto
    {
       List<Producto> All();
       List<Producto> AllNombre(string criterio);
       Producto Find(int id);
       void Store(Producto producto);
       void Update(Producto producto);
       void Delete(int id);

       List<Producto> ByQueryAll(string query);
    }
}
