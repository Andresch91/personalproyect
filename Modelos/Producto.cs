﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Modelos
{
   public class Producto
    {
        public int IdProducto { get; set; }
        public string Nombre { get; set; }
        public decimal Precio { get; set; }
        public string Marca { get; set; }
        public decimal Stock { get; set; }

        public int IdCategoriafk { get; set; }
        public Categoria Categoria { get; set; }
    }
}
