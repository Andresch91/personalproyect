﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Modelos
{
   public class Venta
    {
        public int IdVenta { get; set; }
        public string TipoComprobante { get; set; }
        public string NroComprobante { get; set; }
        public DateTime Fecha { get; set; }
        public double Total { get; set; }

        public int IdClientefk { get; set; }
        public Cliente Cliente { get; set; }

        public virtual List<DetalleVenta> Detalleventa { get; set; }
    }
}
