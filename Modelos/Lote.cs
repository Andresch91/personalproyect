﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Modelos
{
   public class Lote
    {
        public int IdLote { get; set; }
        public DateTime FechaVencimiento { get; set; }
        public Int32 NroLote { get; set; }
        public Int32 StockInicial { get; set; }
        public int Idproductofk { get; set; }
        public Producto Producto { get; set; }

        public int IdProveedorfk { get; set; }
        public Proveedor Proveedor { get; set; }

    }
}
