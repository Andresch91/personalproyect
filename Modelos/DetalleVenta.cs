﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

namespace Modelos
{
   public class DetalleVenta
    {

        public int IdDetalleVenta { get; set; }
        public int Cantidad { get; set; }
        public double PrecioDv { get; set; }
        public int StockModificar { get; set; }

        public int IdVentafk { get; set; }
        public Venta Venta { get; set; }

        public int IdProductofk { get; set; }
        public virtual Producto Producto { get; set; }
    }
}
