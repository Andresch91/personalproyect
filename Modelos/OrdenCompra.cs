﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Modelos
{
   public class OrdenCompra
    {
        public int IdOrdenCompra { get; set; }
        public int Cantidad { get; set; }
        public decimal Precio { get; set; }

        public int IdProductofk { get; set; }
        public Producto Producto { get; set; }
    }
}
