﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

namespace Modelos
{
   public class Proveedor
    {
        public int IdProveedor { get; set; }
        public string Ruc { get; set; }
        public string RazonSocial { get; set; }
        public string Telefono { get; set; }
       // public string Ciudad { get; set; }
        public string Correo { get; set; }
        public string PaginaWeb { get; set; }
    }
}
