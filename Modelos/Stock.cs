﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Modelos
{
   public class Stock
    {
        public int IdStock { get; set; }
        public Int32 StockActual { get; set; }
        public int Idproductofk { get; set; }
        public int IdLotefk { get; set; }

        public Producto Producto { get; set; }
        public Lote Lote { get; set; }
    }
}
